//
//  AppDelegate.h
//  KonK
//
//  Created by aa on 7/13/14.
//  Copyright (c) 2014 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

