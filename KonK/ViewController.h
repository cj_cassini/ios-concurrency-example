//
//  ViewController.h
//  KonK
//
//  Created by aa on 7/13/14.
//  Copyright (c) 2014 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSZipArchive.h"
#import "MyCollectionViewCell.h"

#define ZIPPED_IMAGES_FILE_NAME_WITHOUT_EXT @"some_pix"

@interface ViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate> {
  BOOL unzippingHasStarted;
  BOOL unzippingHasFinished;
  NSString* documentsDirectory;
  NSString* destinationPath;
  CGFloat imageSizeMult;
}
@property IBOutlet UILabel* infoLabel;
@property (strong, nonatomic) NSMutableArray* imagePathsArray;
@property (strong, nonatomic) NSMutableArray* imageArray;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

-(IBAction) buttonWasPressed: (id) sender;
-(IBAction) sliderWasMoved:(id)sender;
@end

