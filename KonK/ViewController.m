//
//  ViewController.m
//  KonK
//
//  Created by aa on 7/13/14.
//  Copyright (c) 2014 aa. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
-(void) setup;
-(void) unzip;
-(void) checkContentsOfUnzippedDirectory;
-(BOOL) fileNameIsForAnImage: (NSString*) path;
-(BOOL) addImageIfNeededWithFileName: (NSString*) path;

@end

@implementation ViewController

-(void) setup {
  
  NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  documentsDirectory = [paths objectAtIndex:0];
  
  self.imageArray = [[NSMutableArray alloc] initWithCapacity: 10];
  self.imagePathsArray = [[NSMutableArray alloc] initWithCapacity: 10];
  
  unzippingHasStarted = NO;
  unzippingHasFinished = NO;
  
  destinationPath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, ZIPPED_IMAGES_FILE_NAME_WITHOUT_EXT];
  NSFileManager* fileManager = [NSFileManager defaultManager];
  
  // if this app is run multiple times, the pics from the last time will still be there
  [fileManager removeItemAtPath:destinationPath error: nil];
  
  UICollectionViewFlowLayout* newLayout = [[UICollectionViewFlowLayout alloc] init];
  newLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
  newLayout.minimumInteritemSpacing = 2.0;

  [self.collectionView setCollectionViewLayout: newLayout];
  
  imageSizeMult = 1.0;
}

// data source
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
  return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return [self.imageArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  MyCollectionViewCell* thisCell = [collectionView dequeueReusableCellWithReuseIdentifier: @"MyCell" forIndexPath: indexPath];
  UIImage* thisImage = self.imageArray[indexPath.row];
  thisCell.imageView.image = thisImage;
  return thisCell;
}

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  CGSize imageSize = ((UIImage*)self.imageArray[indexPath.row]).size;
  CGSize thisSize = CGSizeMake(imageSize.width * imageSizeMult, imageSize.height * imageSizeMult);
  return thisSize;
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

}


-(IBAction) buttonWasPressed: (id) sender {
  
  if(unzippingHasStarted) {return;};
  
  unzippingHasStarted = YES;
  
  [self checkContentsOfUnzippedDirectory];
  unzippingHasFinished = NO;
  
  dispatch_queue_t myQueue = dispatch_queue_create("com.chrisjeffs.unzip_queue", DISPATCH_QUEUE_SERIAL);

  dispatch_async(myQueue, ^{
    [self unzip];
  });

  dispatch_async(myQueue, ^{
    self.infoLabel.text = @"unzipping finished";
    unzippingHasFinished = YES;
  });

}

-(IBAction) sliderWasMoved:(id)sender {
  CGFloat sliderVal = ((UISlider*)sender).value;
  imageSizeMult = sliderVal;
  // try to keep item centered as the sizes are changed
  NSIndexPath* thisIndexPath;
  CGPoint trialPoints[] = {
    CGPointMake(CGRectGetMidX(self.collectionView.bounds), CGRectGetMidY(self.collectionView.bounds)),
    CGPointMake(CGRectGetMidX(self.collectionView.bounds), self.collectionView.bounds.size.height * 0.25),
    CGPointMake(CGRectGetMidX(self.collectionView.bounds), self.collectionView.bounds.size.height * 0.75)
  };
  for(int i = 0; i < 3; i++) {
    thisIndexPath = [self.collectionView indexPathForItemAtPoint: trialPoints[i]];
    if(thisIndexPath) {
      break;
    };
  };
  [self.collectionView reloadData];
  if(thisIndexPath) {
    [self.collectionView scrollToItemAtIndexPath: thisIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated: NO];
  };
}

-(void) checkContentsOfUnzippedDirectory {
  dispatch_queue_t thisQueue = dispatch_queue_create("com.chrisjeffs.report_queue", DISPATCH_QUEUE_SERIAL);
  dispatch_async(thisQueue, ^{
    
    NSFileManager* filemanager = [NSFileManager defaultManager];
    int i = 0;
    while (!unzippingHasFinished) {
      NSArray* pathsNow = [filemanager contentsOfDirectoryAtPath: destinationPath error: nil];

      for(NSString* thisFileName in pathsNow) {
        if([self fileNameIsForAnImage: thisFileName]) {
          if([self addImageIfNeededWithFileName: thisFileName]) {
            dispatch_async(dispatch_get_main_queue(), ^{
              [self.collectionView reloadData];
            });
          };
        };
      };

      i++;
      sleep(1);
    };
    
  });
}
            
-(BOOL) fileNameIsForAnImage: (NSString*) path {
  NSString* pathExtension = path.pathExtension;
  pathExtension = [pathExtension lowercaseString];

  if([pathExtension containsString: @"tiff"]) {return YES;};
  if([pathExtension containsString: @"tif"]) {return YES;};
  if([pathExtension containsString: @"jpg"]) {return YES;};
  if([pathExtension containsString: @"jpeg"]) {return YES;};
  return NO;
}

-(BOOL) addImageIfNeededWithFileName: (NSString*) fileName {
  
  NSString* filePath = [NSString stringWithFormat: @"%@/%@", destinationPath, fileName];
  
  if(![self.imagePathsArray containsObject: filePath]) {
    //NSLog(@"filePath is %@", filePath);
    [self.imagePathsArray addObject: filePath];
    UIImage* thisImage = [UIImage imageWithContentsOfFile: filePath];
    
    if(thisImage) {
      [self addImageToArray: thisImage];
      return YES;
    } else {
      //NSLog(@"image didn't get created");
      return NO;
    };
  };
  return NO;
}

-(void) addImageToArray: (UIImage*) thisImage {
  
  static const CGFloat fixedWidth = 120.0;
  CGFloat sizeRatio = thisImage.size.height / thisImage.size.width;
  CGFloat thisHeight = fixedWidth * sizeRatio;
  
  UIGraphicsBeginImageContext(CGSizeMake(fixedWidth, thisHeight));
  [thisImage drawInRect:CGRectMake(0.0, 0.0, fixedWidth, thisHeight)];
  UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  [self.imageArray addObject: scaledImage];
}

-(void) unzip {
  NSString* zipPath = [[NSBundle mainBundle] pathForResource: ZIPPED_IMAGES_FILE_NAME_WITHOUT_EXT ofType:@"zip"];
  
  [SSZipArchive unzipFileAtPath:zipPath toDestination: documentsDirectory];
  
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view, typically from a nib.

  [self setup];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
