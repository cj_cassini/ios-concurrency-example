//
//  MyCollectionViewCell.h
//  KonK
//
//  Created by aa on 7/13/14.
//  Copyright (c) 2014 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
