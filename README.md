# README #

This is just a small example to demonstrate how to put something on a serial GCD queue :)

The app expects there to be a zip with some images in the project, as I didn't check it in you'll have to do it yourself - see the #define in line 13 of ViewController.h.

Uses SSZipArchive - https://github.com/soffes/ssziparchive